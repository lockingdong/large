<?php
/**
  Template Name: Home Page
 */
remove_filter('acf_the_content', 'wpautop');
get_header(); ?>


<?php 

  $url_array = array();
  $slider_images = acf_photo_gallery('home_banner_slider', $post->ID);

?>

<header id="section_header">
<div class="slider-wrap">
<div class="slider main-slider">
  <?php if(count($slider_images) ): ?>
    <?php foreach ($slider_images as $image): ?>
    <div>
      <div class="img-wrap">
        <img src="<?php echo $image['full_image_url'];?>" alt="">
      </div>
    </div>
    <?php endforeach; ?>
  <?php endif; ?>
</div>
</div>
</header>

<section id="section_banner3">
<div class="img-wrap">
  <img src="<?php echo get_field("home_banner_2")["url"]; ?>" alt="">
</div>
</section>

<section id="section_banner3">
<div class="img-wrap">
  <img src="<?php echo get_field("home_banner_3")["url"]; ?>" alt="">
</div>
</section>
<section id="video">
<div class="container">
  <div class="row">
    <div class="col-12 col-lg-6">
      <div class="video-wrap">
        <iframe src="<?php echo get_field("home_video_url"); ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
      </div>
    </div>
    <div class="col-12 col-lg-6">
      <div class="content-wrap">
        <div class="title-area">
          <div class="logo-wrap">
            <img src="<?php bloginfo("stylesheet_directory"); ?>/src/dist/img/logo.png" alt="">
          </div>
          <h2><?php echo get_field("home_video_title"); ?></h2>
        </div>
        <div class="content">
          <p>
            <?php echo get_field("home_video_text"); ?>
          </p>
        </div>
        <div class="read-more-btn">
          <span class="open-btn">▼ 展開全部</span>
          <span class="close-btn" style="display: none">▲ 收回內容</span>
          
        </div>
      </div>
    </div>
  </div>
</div>
</section>

<!-- ▲ -->




<?php if( have_rows('proxy_product') ): ?>


  <?php while( have_rows('proxy_product') ): the_row(); 

    // vars
    $name = get_sub_field('proxy_name');
    $id = get_sub_field('proxy_id');
    $image = get_sub_field('proxy_image');
    $logo = get_sub_field('proxy_logo');
    $link = get_sub_field('proxy_url');

    ?>


    <section class="section_brand"
             name = "<?php echo $name; ?>" 
             id="<?php echo $id; ?>" 
             style="background-image: url('')"
    >
      <a href="<?php echo $link; ?>" target="_blank">
        <div class="bg-img">
          <img src="<?php echo $image['url']; ?>" alt="">
        </div>
        <div class="blur-glass">
          <div class="bg-img">
            <img src="<?php echo $image['url']; ?>" alt="">
          </div>
          
        </div>
        
        <div class="logo-area">
          <img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>">
        </div>
      </a>
    </section>

  <?php endwhile; ?>


<?php endif; ?>
<section id="news-section">
  <div class="container">
    <div class="row">
      <div class="title-wrap">
        <h2><?php echo get_field("blog_home_title"); ?></h2>
      </div>
    </div>
    
    <div class="row">






      <?php
           // Define our WP Query Parameters 
             $query_options = array(
                 'category_name' => 'home',
                 'posts_per_page' => 3,
             );
             $the_query = new WP_Query( $query_options ); 

             while ($the_query -> have_posts()) : $the_query -> the_post(); 
          ?>


      <div class="col-12 col-md-4 post-col">
        <div class="content-block">
          <a href="<?php echo the_permalink(); ?>">
            <div class="img-wrap">



  <?php 
    $post_thumbnail_id = get_post_thumbnail_id( $post->ID );
    if(!empty($post_thumbnail_id)) :?>
    <?php $img_ar =  wp_get_attachment_image_src( $post_thumbnail_id, 'full' ); ?>
    <?php $img_alt =  get_post_meta($post_thumbnail_id, '_wp_attachment_image_alt', true); ?>
    <img src="<?php echo $img_ar[0];?>"
       alt="<?php echo $img_alt;?>"
    />
  <?php endif; ?>



              <!-- <img src="src/dist/img/news/pic-2.png" alt=""> -->
            </div>
            <!-- <?php 
              //$terms = get_the_terms( $post ->ID, 'type');
              //if(!is_wp_error($terms) && $terms):
                //foreach ($terms as $term):

            ; ?>
            <span><?php //echo esc_html($term->name);?></span>
            <?php 
              //endforeach;
            //endif;
            ; ?> -->





            <div class="txt">
              <div class="title">
                <h5 class="custom-h5"><?php the_title(); ?></h5>
              </div>
              <div class="date">
                <i class="far fa-calendar-alt"></i>
                <time datetime="2018-12-11"><?php echo get_the_date( 'Y. m. d' ); ?></time>
              </div>
              <div class="cate">
                <?php the_category(' '); ?>
              </div>
              
              <!-- <p><?php //echo get_the_ID(); ?></p> -->
              <div class="content-p">
                <?php echo wp_trim_words(get_field("news_content"), 95, '...' ); ?>
              <a href="<?php echo the_permalink(); ?>"><span style="color:#fe925f;font-size:14px;font-weight:300;">Read more</span></a>
                
              </div>
            </div>
          </a>
        </div>
      </div>


        

      <?php 
             endwhile;
             wp_reset_postdata();
        ?>
      
    </div>
    <div class="row">
      <div class="sub-area">
        <a href="<?php echo get_page_link(297); ?>">
          <button>SEE MORE</button>
        </a>
      </div>
    </div>

    
  </div>
</section>


<!-- <section id="contact"
       style="background-image: url('<?php bloginfo("stylesheet_directory"); ?>/src/dist/img/home/contact-bg.png')"
>




<div class="container">
  <div class="row">
    <div class="col-12">
      <div class="form-wrap">
        <h2>聯絡我們</h2>

        <?php //echo do_shortcode( '[contact-form-7 id="30" title="聯絡表單"]' ); ?>

      </div>
    </div>
  </div>
</div>
</section> -->





<?php
//get_sidebar();
get_footer();
