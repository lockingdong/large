<?php
/**
  Template Name: Contact Page
 */

get_header(); ?>

<section class="section_title">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12 px-0">
        <div class="title_section d-flex justify-content-center">
          <div class="title_section_mask"></div>
          <img src="<?php echo get_field("contact_us_cover_img")["url"]; ?>" alt="<?php echo get_field("contact_us_cover_img")["alt"]; ?>">
          <div class="title_section_wrap">
            <h2 class="custom-h3"><?php echo get_field("contact_us_title"); ?></h2>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="contact-us-form">
  <div class="container">
    <div class="title-wrap">
      <h2><?php echo get_field("form_title"); ?></h2>
    </div>
    <div class="row">
      <div class="col-12">
        <div class="contact-form-wrap">
          <?php echo do_shortcode( '[contact-form-7 id="30" title="聯絡表單"]' ); ?>
          <!-- <form action="">
            <div class="row">
              <div class="col-12 col-lg-6">
                <div class="input-wrap">
                  <input class="input" type="text">
                  <span class="label">First Name</span>
                </div>  
              </div>
              <div class="col-12 col-lg-6">
                <div class="input-wrap">
                  <input class="input" type="text" required>
                  <span class="label">Last Name</span>
                </div>  
              </div>
              <div class="col-12 col-lg-6">
                <div class="input-wrap">
                  <input class="input" type="text" required>
                  <span class="label">Phone</span>
                </div>  
              </div>
              <div class="col-12 col-lg-6">
                <div class="input-wrap">
                  <input class="input" type="text" required>
                  <span class="label">Email</span>
                </div>  
              </div>
              <div class="col-12">
                <div class="input-wrap">
                  <input class="input" type="text" required>
                  <span class="label">Title</span>
                </div>  
              </div>
              <div class="col-12">
                <div class="input-wrap">
                  <textarea class="textarea" name="" id="" cols="30" rows="10" required></textarea>
                  <span class="label">Your Message</span>
                </div>  
              </div>
            </div>
            <div class="sub-area">
              <button type="submit">SUBMIT</button>
            </div>  
          </form> -->
        </div>
      </div>
    </div>
  </div>
</section>

  
<section id="form-location-section">
  <div class="container-fluid">
      <div class="row">
        <div class="col-12 col-lg-6">
          <div class="wrap">
            <div class="content-wrap">
              <div class="title-area ">
                <h5 class="custom-h5"><?php echo get_field("company_name"); ?></h5>
              </div>
              <div class="content">
                <ul class="list-unstyled">
                  <?php if( have_rows('company_infos') ): ?>
                    <?php while( have_rows('company_infos') ): the_row(); ?>


                      <li>
                        <span><?php echo get_sub_field('info_title'); ?></span>
                        <span><?php echo get_sub_field('info_content'); ?></span>
                      </li>


                    <?php endwhile; ?>
                  <?php endif; ?>
                </ul>
          
              </div>
          
              </div>
            </div>
          </div>
          <div class="col-12 col-lg-6">
          <div class="map-wrap">
            <?php echo get_field("google_map_code"); ?>
          </div>
        </div>
    </div>
  </div>
</section>


<!-- <section id="location-bg">
  <div class="img-wrap">
    <div class="bg-mask"></div>
    <img src="src/dist/img/location/bg-img.png" alt="">
  </div>
</section>
 -->


<?php
//get_sidebar();
get_footer();