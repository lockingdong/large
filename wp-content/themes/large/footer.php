<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ken-cens.com
 */

?>
<footer id="section_footer">
    <div class="container">
      <div class="row">
        <div class="col-12 col-lg-3">
          <div class="logo-wrap">
            <img src="<?php bloginfo("stylesheet_directory"); ?>/src/dist/img/logo.png" alt="">
          </div>
          <div class="footer-media-wrap">
            <?php if( have_rows('footer_media_links', 6) ): ?>
              <?php while( have_rows('footer_media_links', 6) ): the_row(); ?>
                <a href="<?php echo get_sub_field('link', 6); ?>" target="_blank">
                  <span class="media-icon <?php echo get_sub_field('icon', 6); ?>"></span>
                </a>

                
              <?php endwhile; ?>
            <?php endif; ?>
          </div>
          
        </div>
        <div class="col-6 col-lg-3">
          <div class="list-wrap">


            <?php if( is_active_sidebar("footer-left")): ?>
              <?php dynamic_sidebar("footer-left"); ?>
            <?php endif; ?>




            <!-- <ul>
              <div class="list-title">SHOPPING</div>
              <li><a href="">Yahoo</a></li>
              <li><a href="">蝦皮</a></li>
            </ul> -->
          </div>
        </div>
        <div class="col-6 col-lg-3">
          <div class="list-wrap">

            <?php if( is_active_sidebar("footer-middle")): ?>
              <?php dynamic_sidebar("footer-middle"); ?>
            <?php endif; ?>

          </div>
        </div>
        <div class="col-12 col-lg-3">
          <div class="address-wrap">


            <?php if( is_active_sidebar("footer-right")): ?>
              <?php dynamic_sidebar("footer-right"); ?>
            <?php endif; ?>


          </div>
          
        </div>
      </div>
    </div>


    
    <div class="cpr-area">
      <!-- Copyright © <strong>MOVEON-DESIGN</strong> All right reserved 2018 -->

      <?php if( is_active_sidebar("copyright")): ?>
        <?php dynamic_sidebar("copyright"); ?>
      <?php endif; ?>
    </div>
  </footer>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.16/vue.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="<?php bloginfo("stylesheet_directory"); ?>/src/dist/js/slick.js"></script>
<script src="<?php bloginfo("stylesheet_directory"); ?>/src/dist/js/all.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>
<?php wp_footer(); ?>

</body>
</html>
