<?php
/**
  Template Name: location Page
 */

get_header(); ?>

<?php //echo get_field("home_video_url"); ?>

<style>
  body {
    background: #f9f9f9;
  }
</style>

<section class="section_title">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12 px-0">
        <div class="title_section d-flex justify-content-center">
          <div class="title_section_mask"></div>
          <img src="<?php echo get_field("loc_cover_img")['url']; ?>" alt="<?php echo get_field("loc_cover_img")['alt']; ?>">
          <div class="title_section_wrap">
            <h3 class="custom-h3"><?php echo get_field("loc_cover_title"); ?></h3>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
  







<?php if( have_rows('dealers') ): ?>
  <?php while( have_rows('dealers') ): the_row(); ?>

<section id="location-section">
  <div class="container">
      <div class="row">
        <div class="col-12 col-lg-6">
          <div class="content-wrap d-flex flex-column justify-content-center">
            <div class="title-area ">
              <h5 class="custom-h5"><?php echo get_sub_field("dealer_title"); ?></h5>
            </div>
            <div class="content">
                <ul class="list-unstyled">

                  <?php if( have_rows('company_infos') ): ?>
                    <?php while( have_rows('company_infos') ): the_row(); ?>


                      <li>
                        <span><?php echo get_sub_field('info_title'); ?></span>
                        <span>

                          
                          <?php if (get_sub_field('is_link')): ?>
                            <a href="<?php echo get_sub_field('info_content'); ?>" target="_blank">
                              Click Here
                            </a>
                          <?php else: ?>
                            <?php echo get_sub_field('info_content'); ?>
                          <?php endif; ?>


                            
                        </span>
                      </li>


                    <?php endwhile; ?>
                  <?php endif; ?>

                </ul>
        
            </div>
        
            </div>
        </div>

      <div class="col-12 col-lg-6">
        <div class="map-wrap">
          <?php echo get_sub_field("google_map_code"); ?>
        </div>
      </div>
    </div>
  </div>
</section>


  <?php endwhile; ?>
<?php endif; ?>










<section id="location-bg">
  <div class="img-wrap">
    <div class="bg-mask"></div>
    <img src="<?php bloginfo("stylesheet_directory"); ?>/src/dist/img/location/bg-img.png" alt="">
  </div>
</section>





<?php
//get_sidebar();
get_footer();
