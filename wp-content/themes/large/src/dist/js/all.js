$(".proxy-product").attr("id", "proxy-link");

var brnad_name = $(".proxy-product a").text();

//console.log(brnad_name);

$(".proxy-product").html(`
    <a class="nav-link dropdown-toggle" id="dropdown" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">${brnad_name}</a>
    <my_link ref="component1"></my_link>
`);

var brands = [];

Vue.component('my_link', {
    template: `
      <div class="dropdown-menu" aria-labelledby="dropdown06">
        <a v-for="brand in brands" class="dropdown-item" :href="'?page_id=6#'+brand.proxy_id">{{brand.proxy_name}}</a>
      </div>
    `,
    data(){
      return {
        //hash: "#",
        brands: brands
      }
      
    }
});


var app = new Vue({
  el: "#proxy-link",
  data: {
    brands: brands
  },
  mounted(){
    var sel = this;

      $.ajax({
      url: "/wp-json/acf/v3/pages/6/proxy_product",
      type: "GET",
      dataType: "json",
      success: function(data){
          sel.brands = data.proxy_product;
          brands = data.proxy_product;
          sel.$refs.component1.brands = data.proxy_product;
          //alert()
          //console.log(sel.brands);

      }
    });

  }
});

// $(".proxy-product").html(`

//     <a class="nav-link dropdown-toggle" id="dropdown" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">代理產品</a>
//     <div class="dropdown-menu" aria-labelledby="dropdown06">
//       <a v-for="brand in brands" class="dropdown-item" href="#3D-Design">{{brand.name}}</a>
       
//     </div>


// `);

$("#wpcf7-f30-o1 input, #wpcf7-f30-o1 textarea").focusin(function(){
  $(this).parent().siblings(".label").addClass("label-change");
}).focusout(function(){

  if(!$(this).val()){
    $(this).parent().siblings(".label").removeClass("label-change");
  }

  
});

$("#wpcf7-f30-o1 textarea").focusin(function(){
  $(this).parent().siblings(".label").addClass("message-change");
}).focusout(function(){
  if(!$(this).val()){
    $(this).parent().siblings(".label").removeClass("message-change");
  }
});

$(".post-name input").val(title);
//=============limit_text================
function limit_text(text_obj, limit_num){
  $(text_obj).each(function(){
  //alert();
  //console.log($(this).text().length);
        var x = $(this).html().replace(/ /g,"");
        var y = x.replace(/\n/g,"");
    //var h3_number = $(this).html().length;
        var h3_number = y.length;
    if(h3_number > limit_num){
      //var text_board_right_limit_h3 = $(this).html().substring(0, limit_num);
            var text_board_right_limit_h3 = y.substring(0, limit_num);
      //console.log(text_board_right_limit_h3)
            if(text_board_right_limit_h3.slice(-1)=="<"){
                text_board_right_limit_h3 = text_board_right_limit_h3.slice(0, -1)
            }
      $(this).html(text_board_right_limit_h3 + '...<span style="color:#fe925f;font-size:14px;font-weight:300;">see more</span>');
    }
        //console.log(y)
  });
}



//=============home================
//main-news-p
//limit_text(".main-news-p", 65);
//limit_text(".title .custom-h5", 15);
//limit_text(".content-p", 65);
$(".fb-link a").html(`<div class="fb-icon-bg"><i class="fab fa-facebook-f"></i></div>`);
$(".ig-link a").html(`<div class="fb-icon-bg"><i class="fab fa-instagram"></i></div>`);


if($(".cate a").text() === '顯示於首頁'){
  $(this).css("display", "none");
}


$(".cate a").each(function(){

  //console.log($(this).text());
  if($(this).text() == '顯示於首頁'){
    
    $(this).css("display", "none");
  }

});




$("section#video .read-more-btn").click(function(){
  $(".read-more-btn span").toggle();

  $("section#video .content").toggleClass("opened");
  $(this).toggleClass("opened");
});

//$(".page-id-6 .content-block").css("height", "90%")



//==================================
$(".slider.main-slider").slick({
  arrows: false,
  autoplay: true,
  dots: true

});

$('.product.slider-for').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: true,
  speed: 500,
  fade: false,
  asNavFor: '.slider-nav',
  prevArrow: "<img class='a-left control-c prev slick-prev' src='" + dir + "/src/dist/img/arrow-left.png'>",
  nextArrow: "<img class='a-right control-c next slick-next' src='"+ dir + "/src/dist/img/arrow-right.png'>"
});
$('.product.slider-nav').slick({
  slidesToShow: 5,
  //slidesToScroll: 1,
  asNavFor: '.slider-for',
  //dots: false,
  focusOnSelect: true
  //centerMode: true
});


$(".product.slider.slider-nav .slick-track").css("transform", "translate3d(0px, 0px, 0px)");
$(window).resize(function () {
  setTimeout(function () {
    $(".product.slider.slider-nav .slick-track").css("transform", "translate3d(0px, 0px, 0px)");
  }, 100);
});


$(".product.slider .slick-slide").on("click", function(){
  setTimeout(function () {
    $(".product.slider.slider-nav .slick-track").css("transform", "translate3d(0px, 0px, 0px)");
  }, 100);
});





//==========================menu click outside================


$('button.navbar-toggler').click(function () {
  //if($(".navbar-collapse").hasClass("show") == true)console.log(1)
  $("header ~ *").on('click', function (e) {
    if ($(".navbar-collapse").hasClass("show") === true) {
      e.preventDefault();
      $(".navbar-collapse").removeClass("show");
    } else {
      return true;
    }
  });
});

//==============================


function scrollIn(obj, aniName, aniTime) {
  $(obj).css("opacity", "0");
  $(window).scroll(function () {
    $(obj).each(function () {
      if ($(window).scrollTop() + $(window).height() >= $(this).offset().top) {
        var self = this;
        setTimeout(function () {
          $(self).addClass('animated ' + aniName).css("opacity", "1");
        }, aniTime);
      }
    });
  });
  var time = 0;
  $(obj).each(function () {
    if ($(window).scrollTop() + $(window).height() > $(this).offset().top) {
      var self = this;
      setTimeout(function () {
        $(self).addClass('animated ' + aniName).css("opacity", "1");;
      }, time);
      time += aniTime;
    }
  });
}

scrollIn("section, header, footer", "fadeIn", 100);



setTimeout(function(){
 $("body").css("padding-top", $(".navbar").height() + "px");
 
}, 100);


//========================
$(document).ready(function(){
    $(".fb-btn").on("click", function(e){
    e.preventDefault();
    //alert();
    window.open('http://www.facebook.com/share.php?u=' + plink);

  });

    $("#phone").mask("9999999999", {});

});


//=======================
//$('#phone').mask('0000-0000');

//alert()
setTimeout(function(){
 $('body').css("padding-top", `${$('nav').outerHeight()}px`);
 
}, 100);



$(".txt p iframe").wrap("<span class='rwd-container'></span>");

$(".ztb-customchatbox-logo").css("opacity", "0");
$(".ztb-customchatbox-logo").css("pointer-events", "none");






