      <footer id="section_footer">
        <div class="container">
          <div class="row">
            <div class="col-12 col-lg-3">
              <div class="logo-wrap">
                <img src="src/dist/img/logo.png" alt="">
              </div>
            </div>
            <div class="col-6 col-lg-2">
              <div class="list-wrap">
                <ul>
                  <div class="list-title">SHOPPING</div>
                  <li><a href="">Yahoo</a></li>
                  <li><a href="">蝦皮</a></li>
                </ul>
              </div>
            </div>
            <div class="col-6 col-lg-2">
              <div class="list-wrap">
                <ul>
                  <div class="list-title">PRODUCT</div>
                  <li><a href="">BMW</a></li>
                  <li><a href="">M-Benz</a></li>
                  <li><a href="">Porsche</a></li>
                  <li><a href="">Super Cars</a></li>
                </ul>
              </div>
            </div>
            <div class="col-12 col-lg-5">
              <div class="address-wrap">
                <ul>
                  <div class="list-title">樂駒國際有限公司</div>
                  <li>地址：104台北市中山區民族東路430號1樓</li>
                  <li>Email：sales@large-automotive.com</li>
                  <li>電話：02 7728-5666</li>
                  <li>傳真：02 2507-7228</li>
                </ul>
              </div>
              
            </div>
          </div>
        </div>
        <div class="cpr-area">
          Copyright © <strong>MOVEON-DESIGN</strong> All right reserved 2018
        </div>
      </footer>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="src/dist/js/slick.js"></script>
    <script src="src/dist/js/all.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.js"></script>
  </body>
</html>