<!DOCTYPE html>
<html lang="zh-Hant-TW">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="format-detection" content="telephone=no">
    <meta name="format-detection" content="address=no">

    
    <title>large</title>
    <!-- Bootstrap core CSS-->
    <!--link(href='vendor/bootstrap/css/bootstrap.css', rel='stylesheet')-->
    <!-- Custom styles for this template-->
    <!--link(href='vendor/bootstrap/css/bootstrap2.css', rel='stylesheet')-->
    <!--link(href='css/bootstrap337.css', rel='stylesheet')-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.css">
    <link href="src/dist/css/all.min.css" rel="stylesheet">
    <link rel="stylesheet prefetch" href="src/dist/css/slick.css">
    <link rel="stylesheet prefetch" href="src/dist/css/slick-theme.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="src/css/public.css">


    <!--link(href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css' rel='stylesheet prefetch')-->
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-light fixed-top nav-shadow">
      <div class="container-fluid"><a class="navbar-brand" href="http://test.moveon-design.com/large/index.php">
        <img src="src/dist/img/logo.png" alt="">
      </a>
        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarsExample04" aria-controls="navbarsExample04" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
        <div class="navbar-collapse collapse" id="navbarsExample04">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item"><a class="nav-link" href="http://test.moveon-design.com/large/about.php" lecture="class1">關於我們</a></li>
            <li class="nav-item"><a class="nav-link" href="http://test.moveon-design.com/large/news-list.php" lecture="class2">最新消息</a></li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" id="dropdown" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">代理產品</a>
              <div class="dropdown-menu" aria-labelledby="dropdown06">
                <a class="dropdown-item" href="http://test.moveon-design.com/large/index.php#3D Design">3D Design</a>
                <a class="dropdown-item" href="http://test.moveon-design.com/large/index.php#DINAN">DINAN</a>
                <a class="dropdown-item" href="http://test.moveon-design.com/large/index.php#AWRON">AWRON</a>
              </div>
            </li>
            <li class="nav-item"><a class="nav-link" href="http://test.moveon-design.com/large/location.php">經銷據點</a></li>
            <li class="nav-item"><a class="nav-link" href="http://test.moveon-design.com/large/index.php#contact">聯絡我們</a></li>
            <li class="nav-item"><a class="nav-link" href="">
              <div class="fb-icon-bg">
                <i class="fab fa-facebook-f"></i>
              </div>
            </a></li>

          </ul>
        </div>
      </div>
    </nav>