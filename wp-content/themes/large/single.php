<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package ken-cens.com
 */

get_header(); ?>
<script>
	(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/zh_TW/sdk.js#xfbml=1&version=v3.0';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));



//alert(title);



</script>

<div id="fb-root"></div>


<section class="section_title">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12 px-0">
				<div class="title_section d-flex justify-content-center">
					<div class="title_section_mask"></div>
					<img src="<?php echo get_field("blog_post_cover", 297)["url"]; ?>" alt="<?php echo get_field("blog_post_cover", 297)["alt"]; ?>">
					<div class="title_section_wrap">
						<h3 class="custom-h3"><?php echo get_field("blog_post_title", 297); ?></h3>

					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<section id="news-post">
	<div class="container">
		<div class="row">
			<div class="col-12 col-lg-8">
				<div class="content-post">
					<div class="title">
						<h4 class="custom-h4"><?php echo get_the_title( $post_id ); ?>

</h4>
					</div>
					<div class="date">
						<i class="far fa-calendar-alt"></i>
						<time datetime="2018-12-11"><?php echo get_the_date( 'Y. m. d' ); ?></time>
					</div>
					<div class="cate">
						<?php the_category(' '); ?>
					</div>
					<div class="img-wrap">



	<?php 
		$post_thumbnail_id = get_post_thumbnail_id( $post->ID );
		if(!empty($post_thumbnail_id)) :?>
		<?php $img_ar =  wp_get_attachment_image_src( $post_thumbnail_id, 'full' ); ?>
		<?php $img_alt =  get_post_meta($post_thumbnail_id, '_wp_attachment_image_alt', true); ?>
		<img src="<?php echo $img_ar[0];?>"
			 alt="<?php echo $img_alt;?>"
		/>
	<?php endif; ?>






					</div>
					<div class="txt">
						<?php echo get_field("news_content"); ?>
					</div>
					<div class="social-media d-flex align-items-end">
						<a class="fb-btn" href=""><i class="fab fa-facebook-square fa-2x"></i></a>
						<a href="https://twitter.com/share" target="_blank"><i class="twitter-share-button fab fa-twitter-square fa-2x"></i></a>
						<div class="fb-like" 
							 data-href="<?php get_the_permalink($post->ID); ?>" 
							 data-layout="button" 
							 data-action="like" 
							 data-size="large" 
							 data-show-faces="false" data-share="false" style="height:32px;"></div>
					</div>
				</div>
			</div>

			<div class="col-12 col-lg-4">
				<div class="side-bar">
					<div class="side-news-box">
						<div class="title">最新消息</div>
						<ul class="list-unstyled post-list">



							<?php
							 // Define our WP Query Parameters 
								 $query_options = array(
								     //'category_name' => 'latest-news',
								     'posts_per_page' => 3,
								 );
								 $the_query = new WP_Query( $query_options ); 

								 while ($the_query -> have_posts()) : $the_query -> the_post(); 
							?>
							<li>
								<div><a href="<?php echo get_permalink(); ?>">
									<?php the_title(); ?>
								</a></div>
								<time><?php echo get_the_date( 'Y. m. d' ); ?></time>
							</li>

							<?php 
							     endwhile;
							     wp_reset_postdata();
							?>
						</ul>
					</div>
					






					<div class="side-news-box">
						<div class="title">熱門消息</div>
						<!-- <ul>
							<li>
								<div><a href="">管營信越具國出管營信越具國出</a></div>
								<time>12. 11. 2018</time>
							</li>
							<li>
								<div><a href="">管營信越具國出管營信越具國出</a></div>
								<time>12. 11. 2018</time>
							</li>
							<li>
								<div><a href="">管營信越具國出管營信越具國出</a></div>
								<time>12. 11. 2018</time>
							</li>
						</ul> -->
						<?php if (function_exists('get_most_viewed')): ?>


						<ul class="list-unstyled post-list">
						<?php get_most_viewed("post", 3); ?>
						</ul>


						<?php endif; ?>
					</div>	
				</div>
		</div>
	</div>
</section>
<section class="messege">
	<div class="container">
	  <div class="row">
	    <div class="col-12 px-0">
	      <div class="form-wrap">
	        <h2>留言給我們</h2>
	        <!-- <form action="">
	          <div class="row">
	            <div class="col-12 col-lg-6">
	              <div class="input-wrap">
	                <input type="text" placeholder="First Name">
	              </div>  
	            </div>
	            <div class="col-12 col-lg-6">
	              <div class="input-wrap">
	                <input type="text" placeholder="Last Name">
	              </div>  
	            </div>
	            <div class="col-12 col-lg-6">
	              <div class="input-wrap">
	                <input type="text" placeholder="Email">
	              </div>  
	            </div>
	            <div class="col-12 col-lg-6">
	              <div class="input-wrap">
	                <input type="text" placeholder="Phone">
	              </div>  
	            </div>
	            <div class="col-12">
	              <div class="input-wrap">
	                <input type="text" placeholder="Title">
	              </div>  
	            </div>
	            <div class="col-12">
	              <div class="input-wrap">
	                <textarea placeholder="Message" name="" id="" cols="30" rows="10"></textarea>
	              </div>  
	            </div>
	          </div>
	          <div class="sub-area">
	            <button type="submit">Submit</button>
	          </div>  
	        </form> -->
	        <?php echo do_shortcode( '[contact-form-7 id="310" title="聯絡表單內頁"]' ); ?>





	    
	      </div>
	    </div>
	  </div>
	</div>
</section>



	

<?php
get_footer();
