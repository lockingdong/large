<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ken-cens.com
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="google-site-verification" content="hagdtOLbiOp4YChwVrXYPKv624CC9P1ROgFw5MWpjb4" />

<link rel="icon" type="image/png" href="<?php bloginfo("stylesheet_directory"); ?>/src/dist/img/lg-s-icon.png">
<link rel="profile" href="http://gmpg.org/xfn/11">


<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.css">
<link href="<?php bloginfo("stylesheet_directory"); ?>/src/dist/css/all.min.css" rel="stylesheet">
<link rel="stylesheet prefetch" href="<?php bloginfo("stylesheet_directory"); ?>/src/dist/css/slick.css">
<link rel="stylesheet prefetch" href="<?php bloginfo("stylesheet_directory"); ?>/src/dist/css/slick-theme.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="<?php bloginfo("stylesheet_directory"); ?>/src/css/public.css">





<script>
  var dir = "<?php echo bloginfo("stylesheet_directory"); ?>";

  var plink = "<?php echo get_permalink($post->id); ?>";

  var title = "<?php echo get_the_title( $post_id ); ?>";

  //alert(plink);

</script>
<script>
!function(d,s,id){var js,fjs=
d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?
'http':'https';if(!d.getElementById(id))
{js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';
fjs.parentNode.insertBefore(js,fjs);}}
(document, 'script', 'twitter-wjs');
</script>


<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'ken-cens-com' ); ?></a>

	<!-- ===============header============ -->
	<nav class="navbar navbar-expand-lg navbar-light fixed-top nav-shadow">
      <div class="container-fluid"><a class="navbar-brand" href="<?php echo get_home_url(); ?>">
        <img src="<?php bloginfo("stylesheet_directory"); ?>/src/dist/img/logo.png" alt="">
      </a>
        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarsExample04" aria-controls="navbarsExample04" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>






        <div class="navbar-collapse collapse" id="navbarsExample04">
          <!-- <ul class="navbar-nav ml-auto">
            <li class="nav-item"><a class="nav-link" href="http://test.moveon-design.com/large/about.php" lecture="class1">關於我們</a></li>
            <li class="nav-item"><a class="nav-link" href="http://test.moveon-design.com/large/news-list.php" lecture="class2">最新消息</a></li>


            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" id="dropdown" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">代理產品</a>
              <div class="dropdown-menu" aria-labelledby="dropdown06">
                <a class="dropdown-item" href="http://test.moveon-design.com/large/index.php#3D Design">3D Design</a>
                <a class="dropdown-item" href="http://test.moveon-design.com/large/index.php#DINAN">DINAN</a>
                <a class="dropdown-item" href="http://test.moveon-design.com/large/index.php#AWRON">AWRON</a>
              </div>
            </li>



            <li class="nav-item"><a class="nav-link" href="http://test.moveon-design.com/large/location.php">經銷據點</a></li>
            <li class="nav-item"><a class="nav-link" href="http://test.moveon-design.com/large/index.php#contact">聯絡我們</a></li>
            <li class="nav-item"><a class="nav-link" href="">
              <div class="fb-icon-bg">
                <i class="fab fa-facebook-f"></i>
              </div>
            </a></li>

          </ul> -->




          <?php

	    	wp_nav_menu( array(

	    		"theme_location"  => "primary",
	    		"container"       => "ul",
	    		"menu_class"      => "navbar-nav ml-auto",
	    	)); 

	     ?>
        </div>

      </div>
    </nav>


    




















	<div id="content" class="site-content">
