<?php
/**
 * ken-cens.com functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package ken-cens.com
 */

if ( ! function_exists( 'ken_cens_com_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function ken_cens_com_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on ken-cens.com, use a find and replace
	 * to change 'ken-cens-com' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'ken-cens-com', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'ken-cens-com' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'ken_cens_com_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );
}
endif;
add_action( 'after_setup_theme', 'ken_cens_com_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function ken_cens_com_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'ken_cens_com_content_width', 640 );
}
add_action( 'after_setup_theme', 'ken_cens_com_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function ken_cens_com_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'ken-cens-com' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'ken-cens-com' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );


	register_sidebar( array(
		'name'          => esc_html__( '底部shopping連結', 'ken-cens-com' ),
		'id'            => 'footer-left',
		'description'   => esc_html__( 'Add widgets here.', 'ken-cens-com' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );


	register_sidebar( array(
		'name'          => esc_html__( '底部product連結', 'ken-cens-com' ),
		'id'            => 'footer-middle',
		'description'   => esc_html__( 'Add widgets here.', 'ken-cens-com' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<div class="widget-title">',
		'after_title'   => '</div>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( '底部聯絡資訊', 'ken-cens-com' ),
		'id'            => 'footer-right',
		'description'   => esc_html__( 'Add widgets here.', 'ken-cens-com' ),
		'before_widget' => '<section>',
		'after_widget'  => '</section>',
		'before_title'  => '<div class="widget-title">',
		'after_title'   => '</div>',
	) );


	register_sidebar( array(
		'name'          => esc_html__( 'copyright資訊', 'ken-cens-com' ),
		'id'            => 'copyright',
		'description'   => esc_html__( 'Add widgets here.', 'ken-cens-com' ),

	) );

	register_sidebar( array(
		'name'          => esc_html__( '最新消息/熱門消息', 'ken-cens-com' ),
		'id'            => 'new_popular_post',
		'description'   => esc_html__( 'Add widgets here.', 'ken-cens-com' ),

	) );
}
add_action( 'widgets_init', 'ken_cens_com_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function ken_cens_com_scripts() {
	wp_enqueue_style( 'ken-cens-com-style', get_stylesheet_uri() );

	wp_enqueue_script( 'ken-cens-com-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'ken-cens-com-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'ken_cens_com_scripts' );



//=============================add nav-link class================================
function add_menuclass($ulclass) {
   return preg_replace('/<a /', '<a class="nav-link"', $ulclass);
}
add_filter('wp_nav_menu','add_menuclass');

//=============================remove p tag in acf content==============
//remove_filter('acf_the_content', 'wpautop');


//==============add <li> in pagination==============
//attach our function to the wp_pagenavi filter
add_filter( 'wp_pagenavi', 'ik_pagination', 10, 2 );
  
//customize the PageNavi HTML before it is output
function ik_pagination($html) {
    $out = '';
  
    //wrap a's and span's in li's
    $out = str_replace("<div","",$html);
    $out = str_replace("class='wp-pagenavi'>","",$out);
    $out = str_replace("<a","<li><a",$out);
    $out = str_replace("</a>","</a></li>",$out);
    $out = str_replace("<span","<li><span",$out);  
    $out = str_replace("</span>","</span></li>",$out);
    $out = str_replace("</div>","",$out);
  
    return '<div class="pagination pagination-centered">
            <ul>'.$out.'</ul>
        </div>';
}


//=================================





function dong_create_post_type(){

	register_post_type('products',
		array(
			'labels' => array(
				'name' => '代理商品',
				'singular_name' => '代理商品',
			),
			'public' => true,
			'menu_position' => 5,
			'has_archive' => false,
			'rewrite' => true ,
			'show_in_rest' => true,
			'rest_base' => 'products',
			//'taxonomies' => array('category'), //增加分類
			'rest_controller_class' => 'WP_REST_Posts_Controller',
			'supports' => array(
				'title',
				'editor',
				'excerpt',
				'thumbnail',
				'comments',
				
			),
		)
	);

}

        //'rewrite' => array( 'slug' => 'books', 'with_front' => true ),


add_action('init', 'dong_create_post_type', 1);




function dong_create_taxonomies(){
	$labels = array(
		'name' => '顯示於首頁',
		'singular_name' => '顯示於首頁',
	);

	register_taxonomy('location', array('products'),
		array(
			'hierarchical' => true,
			'labels' => $labels,
			'show_ui' => true,
			'show_admin_column' => true,
			'show_in_nav_menus' => true,
		)
	);



	

}
add_action('init', 'dong_create_taxonomies', 0);

//add_filter( 'jetpack_development_mode', '__return_true' );
//add_filter( 'jetpack_is_staging_site', '__return_true' );











/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';
