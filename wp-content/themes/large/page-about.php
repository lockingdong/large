<?php
/**
  Template Name: about Page
 */

get_header(); ?>

<?php //echo get_field("home_video_url"); ?>
<section class="section_title">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12 px-0">
          <div class="title_section d-flex justify-content-center">
            <div class="title_section_mask"></div>
            <img src="<?php echo get_field("about_cover_img")['url']; ?>" alt="<?php echo get_field("about_cover_img")['alt']; ?>">
            <!-- <div class="title_section_wrap">
              <img src="src/dist/img/about/banner_title.png" alt="" class="img-fluid">
            </div> -->
            <div class="title_section_wrap">
              <h3 class="custom-h3"><?php echo get_field("about_cover_title"); ?></h3>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section id="section_content">  
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="txt">
            <p class="text-center txt-title"><?php echo get_field("about_p1_title"); ?></p>
            <?php echo get_field("about_p1_content"); ?>
          </div>

          <div class="img-wrap">
            <img src="<?php echo get_field("about_middle_img")['url']; ?>" alt="<?php echo get_field("about_middle_img")['alt']; ?>">
          </div>

          <div class="txt">
            <?php echo get_field("about_p2_content"); ?>
          </div>

          <div class="img-section d-flex justify-content-between flex-wrap">
        

            <?php if( have_rows('about_img_area') ): ?>
              <?php while( have_rows('about_img_area') ): the_row();?>

                <div class="img-wrap">
                  <img src="<?php echo get_sub_field('about_img')['url']; ?>" alt="">
                </div>


              <?php endwhile; ?>
            <?php endif; ?>

            
          </div>
        </div>
      </div>
    </div>
  </section>





<?php
//get_sidebar();
get_footer();
