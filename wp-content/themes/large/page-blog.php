<?php
/**
  Template Name: Blog Page
 */

remove_filter('acf_the_content', 'wpautop');
get_header(); ?>



	
<section class="section_title">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12 px-0">
				<div class="title_section d-flex justify-content-center">
					<div class="title_section_mask"></div>
					<img src="<?php bloginfo("stylesheet_directory"); ?>/src/dist/img/news/banner_title.png" alt="">
					<div class="title_section_wrap">
						<h3 class="custom-h3">Latest News 最新消息</h3>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>






<section id="news-section">
	<div class="container">

		<div class="row main-news-row" style="display: <?php echo ($paged===0)?"block": "none"; ?>;">


			<div class="col-12 main-news">

				<?php
					 // Define our WP Query Parameters 
						 $query_options = array(
						     //'category_name' => 'latest-news',
						     'posts_per_page' => 1,
						 );
						 $the_query = new WP_Query( $query_options ); 

						 while ($the_query -> have_posts()) : $the_query -> the_post(); 
					?>
				<div class="img-wrap bg-img">
					<div class="bg-mask"></div>



					<?php $thumbnail_id = get_post_thumbnail_id( $post->ID ); ?>
					<?php $image = wp_get_attachment_image_src( $thumbnail_id, 'single-post-thumbnail' ); ?>
					<?php $alt = get_post_meta($thumbnail_id, '_wp_attachment_image_alt', true); ?>
					


					<img src="<?php echo $image[0]; ?>" alt="<?php echo $alt; ?>">
				</div>

				<div class="row">

					


					<div class="col-12  col-sm-12  col-md-6 offset-md-6 content">
						<div class="content-section">
							<div class="title">
								<div class="tag">LATEST NEWS</div>
								<h5 class="custom-h5"><?php the_title(); ?></h5>
								
							</div>

							<p class="main-news-p"><?php the_field("news_content", false, false); ?></p>
							<div class="seemore-area">
                              <a href="<?php echo get_permalink(); ?>">
                                <button class="px-0">SEE MORE</button>
                              </a>
                            </div>
						</div>
					</div>

				</div>
				<?php 
				     endwhile;
				     wp_reset_postdata();
				?>
			</div>
		</div>






		<div class="row" style="margin-top: <?php echo ($paged===0)?"0": "50px"; ?>;">






			<?php if ( have_posts() ) : ?>
	<?php while ( have_posts() ) : the_post(); ?>    

			<div class="col-12 col-sm-6 col-lg-4">
				<div class="content-block">
					<a href="<?php echo the_permalink(); ?>">
						<div class="img-wrap">



	<?php 
		$post_thumbnail_id = get_post_thumbnail_id( $post->ID );
		if(!empty($post_thumbnail_id)) :?>
		<?php $img_ar =  wp_get_attachment_image_src( $post_thumbnail_id, 'full' ); ?>
		<?php $img_alt =  get_post_meta($post_thumbnail_id, '_wp_attachment_image_alt', true); ?>
		<img src="<?php echo $img_ar[0];?>"
			 alt="<?php echo $img_alt;?>"
		/>
	<?php endif; ?>



							<!-- <img src="src/dist/img/news/pic-2.png" alt=""> -->
						</div>
						<!-- <?php 
							//$terms = get_the_terms( $post ->ID, 'type');
							//if(!is_wp_error($terms) && $terms):
								//foreach ($terms as $term):

						; ?>
						<span><?php //echo esc_html($term->name);?></span>
						<?php 
							//endforeach;
						//endif;
						; ?> -->





						<div class="txt">
							<div class="title">
								<h5 class="custom-h5"><?php the_title(); ?></h5>
							</div>
							<div class="date">
								<i class="far fa-calendar-alt"></i>
								<time datetime="2018-12-11"><?php echo get_the_date( 'Y. m. d' ); ?></time>
							</div>
							<div class="cate">
								<?php the_category(' '); ?>
							</div>
							
							<!-- <p><?php //echo get_the_ID(); ?></p> -->
							<p class="content-p">
								<?php the_field("news_content", false, false); ?>
						
								
							</p>
						</div>
					</a>
				</div>
			</div>

				

				<?php endwhile; ?>
<?php endif; ?>
			
		</div>


		<div class="row">
			<div class="col-12">
				<ul class="pagination pagination-sm  justify-content-center custom-pagination">
					<?php //wp_pagenavi(); ?>
				  <!-- <li><a href="#">1</a></li>
				  <li><a href="#">2</a></li>
				  <li class="active"><a href="#">3</a></li>
				  <li><a href="#">4</a></li>
				  <li><a href="#">5</a></li>
				  <li><a href="#">&gt;</a></li> -->
				</ul>
			</div>
		</div>
	</div>
</section>


<?php
//get_sidebar();
get_footer();
