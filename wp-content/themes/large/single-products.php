<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package ken-cens.com
 */

get_header(); ?>

<style>
	body {
		background-color: #f9f9f9;
	}
		
</style>

<section class="section_title">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12 px-0">
				<div class="title_section d-flex justify-content-center">
					<div class="title_section_mask"></div>


					<?php 
						$post_thumbnail_id = get_post_thumbnail_id( $post->ID );
						if(!empty($post_thumbnail_id)) :?>
						<?php $img_ar =  wp_get_attachment_image_src( $post_thumbnail_id, 'full' ); ?>
						<?php $img_alt =  get_post_meta($post_thumbnail_id, '_wp_attachment_image_alt', true); ?>
						<img src="<?php echo $img_ar[0];?>"
							 alt="<?php echo $img_alt;?>"
						/>
					<?php endif; ?>

		
					<div class="title_section_wrap">
						<h3 class="custom-h3"><?php echo get_field("cover_title"); ?></h3>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section id="product-intro">
  <div class="container">
    <div class="row">
      <div class="col-12 col-lg-6">
        <div class="prduct-wrap">
          <img src="<?php echo $img_ar[0];?>"
							 alt="<?php echo $img_alt;?>"
						/>
        </div>
      </div>
      <div class="col-12 col-lg-6">
        <div class="content-wrap d-flex flex-column justify-content-between">
          <div class="title-area">
            <h4 class="custom-h4"><?php echo get_field("product_title"); ?></h4>
          </div>
          <div class="content">
            <div><?php echo get_field("product_sub_title"); ?></div>


            <?php if( have_rows('product_info') ): ?>

				

				<?php while( have_rows('product_info') ): the_row(); 

					// vars
					$title = get_sub_field('info_title');
					$text = get_sub_field('info_text');
					

					?>

					


					<div>
		            	<span><?php echo $title; ?>：</span>
		            	<span><?php echo $text; ?><span>
		            </div>

				<?php endwhile; ?>

			<?php endif; ?>


			<div>
            	<span>價格：</span>
            	<span><?php echo get_field("product_price"); ?></strong><span>
            </div>



          </div>

          <div class="img-wrap">
          	<img src="<?php echo get_field("product_logo")["url"]; ?>" alt="<?php echo get_field("product_logo")["alt"]; ?>">
          </div>
        </div>
      </div>
    </div>
  </div>
</section>


<section id="product-post">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="txt">
					<?php echo get_field("product_content"); ?>
				</div>
				<div class="img-box">






					<?php if( have_rows('product_imgs') ): ?>

				

						<?php while( have_rows('product_imgs') ): the_row(); 

							// vars
							$img = get_sub_field('product_img');
							
							

							?>

							


							<div class="img-wrap">
								<img src="<?php echo $img['url']?>" alt="<?php echo $img['alt']?>">
							</div>

						<?php endwhile; ?>

					<?php endif; ?>
					
					
				</div>

			</div>
		</div>
	</div>
</section>


<section class="messege">
	<div class="container">
	  <div class="row">
	    <div class="col-12 px-0">
	      <div class="form-wrap">
	        <h2>留言給我們</h2>
	        <!-- <form action="">
	          <div class="row">
	            <div class="col-12 col-lg-6">
	              <div class="input-wrap">
	                <input type="text" placeholder="Name">
	              </div>  
	            </div>
	            <div class="col-12 col-lg-6">
	              <div class="input-wrap">
	                <input type="text" placeholder="Email">
	              </div>  
	            </div>
	            <div class="col-12 col-lg-6">
	              <div class="input-wrap">
	                <input type="text" placeholder="Phone">
	              </div>  
	            </div>
	            <div class="col-12 col-lg-6">
	              <div class="input-wrap">
	                <input type="text" placeholder="Title">
	              </div>  
	            </div>
	            <div class="col-12">
	              <div class="input-wrap">
	                <textarea placeholder="Message" name="" id="" cols="30" rows="10"></textarea>
	              </div>  
	            </div>
	          </div>
	          <div class="sub-area">
	            <button type="submit">Submit</button>
	          </div>  
	        </form> -->





	        <form class="wc_comm_form wc_main_comm_form" method="post" enctype="multipart/form-data">
			    
		        <div class="wpd-form-row row">

		        	<div class="wpdiscuz-item wpd-has-icon col-12 col-lg-6">
	                    <div class="input-wrap">
	                    	<input required="required" class="wc_name wpd-field" type="text" name="wc_name" placeholder="Name" maxlength="50" pattern=".{3,50}" title="">
	                    </div>
	                    
	                </div>
		     
		                
	                <div class="wpdiscuz-item wpd-has-icon col-12 col-lg-6">
	                	<div class="input-wrap">
	                		<input value="" required="required" class="wc_email wpd-field" type="email" name="wc_email" placeholder="Email">
	                	</div>
	                </div>
	                
	                <div class="wpdiscuz-item wpd-has-icon col-12 col-lg-6">
	                	<div class="input-wrap">
	                		<input required="required" id="phone" class="custom_field_5b584bb861b46 wpd-field wpd-field-text" type="text" name="custom_field_5b584bb861b46" value="" 
	                		maxlength="10" 
	                		required="required"
	                		placeholder="Phone">
	                	</div>
	                    
	                </div>
	                <div class="wpdiscuz-item col-12 col-lg-6">
	                	<div class="input-wrap">
	                		<input class="custom_field_5b584c07305ad wpd-field wpd-field-text" type="text" name="custom_field_5b584c07305ad" required="required" value="" placeholder="Title">
	                	</div>  
	                </div>
	                
	                <div class="wpdiscuz-item wc-field-textarea col-12">
	                	<div class="input-wrap">
				                <div class="wc-field-avatararea" style="display: block;">
				                                                       
				                </div>
				                
				                	
				                
				                <textarea id="wc-textarea-0_0" placeholder="Message" required="" name="wc_comment"  cols="30" rows="10" class="wc_comment wpd-field"></textarea>
				      
				            
			            </div>
			        </div>
	                <div class="wc-field-submit sub-area" style="width: 100%;">
	              
	                    <button class="wc_comm_submit wc_not_clicked button alt" name="submit" type="submit">Submit</button>
	                    <!-- <input class="wc_comm_submit wc_not_clicked button alt" type="submit" name="submit" value="Post Comment"> -->
	                    <h5 class="suc-ms">我們已收到您的留言</h5>
	                </div>
	                <!-- <div class="wc-field-captcha wpdiscuz-item">
	                    <div class="wc-bin-captcha">
	                        <i class="fas fa-shield-alt"></i>This comment form is under antispam protection                    
	                    </div>
	                </div> -->

		 
		            <div class="clearfix"></div>
		        </div>
			    
			    <div class="clearfix"></div>
			    <input type="hidden" class="wpdiscuz_unique_id" value="0_0" name="wpdiscuz_unique_id">
			</form>
	      </div>
	    </div>
	  </div>
	</div>
</section>



<?php //comments_template(); ?>







	

<?php
get_footer();
